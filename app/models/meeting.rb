class Meeting < ActiveRecord::Base
	validates_presence_of :user_id, :book_id, :start_datetime
	belongs_to :book
	belongs_to :user

	scope :on_date, -> { where(end_datetime: nil) }
	scope :off, -> { where.not(end_datetime: nil) } 

	def on_date?
		end_datetime.nil?
	end

	
end
